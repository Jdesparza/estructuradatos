import java.util.Random;
import java.util.Scanner;

public class JdesparzaEVA_ABB {
    public static int cont;
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        cont = 0;
        boolean rOpcion = true;

        Random numeroAleatorio = new Random();

        ArbolOperacion arbol = new ArbolOperacion();

        System.out.print("Ingrese la cantidad de nodos que quiere que tenga el árbol: ");
        int n = scanner.nextInt();
        int[] valor = new int[n];

        System.out.println("Insertando los siguientes valores '|indice|valor|':");
        for (int i = 0; i < n; i++) {
            valor[i] = numeroAleatorio.nextInt(100);
            System.out.printf("|%d|%d| ", cont, valor[i]);
            arbol.insertarNodo(cont, valor[i]);
            cont++;
        }
        cont = cont-1;
        System.out.println("\n");

        do {
            System.out.println("|-----------------------MENÚ------------------|");
            System.out.println("| 1. Eliminar valor del Arbol                 |");
            System.out.println("| 2. Recorrer Arbol                           |");
            System.out.println("| 3. Salir                                    |");
            System.out.println("|---------------------------------------------|");

            System.out.print("Ingrese una opción: ");
            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.print("Ingrese el indice del Nodo a eliminar del árbol: ");
                    int valorEliminar = scanner.nextInt();

                    if (valorEliminar > cont) {
                        System.out.println("El indice del valor a eliminar no existe");
                    } else {
                        System.out.println("Eliminando Nodo...\nNodo Eliminado con exito.\n\tLos valores del árbol " +
                                "con sus respectivos indices son: ");
                        valor = arbol.eliminarNodo(valorEliminar, valor);
                        cont = valor.length - 1;
                    }
                    break;

                case 2:
                    boolean recorrer = true;
                    do {
                        System.out.println("\n");
                        System.out.println("|------RECORRIDOS------|");
                        System.out.println("| 1. PreOrden          |");
                        System.out.println("| 2. InOrden           |");
                        System.out.println("| 3. PostOrden         |");
                        System.out.println("| 4. Cancelar          |");
                        System.out.println("|----------------------|");
                        System.out.print("Ingrese el recorrido a realizar: ");
                        int opcionRecorrido = scanner.nextInt();

                        switch (opcionRecorrido) {
                            case 1:
                                System.out.println("Recorrido PreOrden");
                                arbol.recorridoPreorden();
                                break;

                            case 2:
                                System.out.println("Recorrido InOrden");
                                arbol.recorridoInorden();
                                break;

                            case 3:
                                System.out.println("Recorrido PostOrden");
                                arbol.recorridoPostorden();
                                break;

                            case 4:
                                System.out.println("SALIDA CON EXITO...");
                                recorrer = false;
                                break;

                            default:
                                System.out.println("Opción incorrecta. Vuelva a ingresar una opción...");
                                break;
                        }
                    } while (recorrer);
                    break;

                case 3:
                    System.out.println("SALIDA DEL PROGRAMA CON EXITO...");
                    rOpcion = false;
                    break;

                default:
                    System.out.println("Opción incorrecta. Vuelva a ingresar una opción...");
                    break;
            }
        } while (rOpcion);
    }
}

class NodoA {
    int indice;
    int datos;
    NodoA nodoIzq;
    NodoA nodoDer;

    public NodoA(int indice, int datos) {
        this.indice = indice;
        this.datos = datos;
        this.nodoIzq = this.nodoDer = null;
    }

    public void insert(int cont, int valorInsertar) {
        if (valorInsertar < datos) {
            if (nodoIzq == null)
                nodoIzq = new NodoA(cont, valorInsertar);
            else
                nodoIzq.insert(cont, valorInsertar);
        } else if (valorInsertar > datos) {
            if (nodoDer == null)
                nodoDer = new NodoA(cont, valorInsertar);
            else
                nodoDer.insert(cont, valorInsertar);
        }
        cont++;
    }
}

class ArbolOperacion {
    private NodoA raiz;

    public ArbolOperacion() {
        raiz = null;
    }

    public void insertarNodo(int cont, int valorInsertar) {
        if (raiz == null)
            raiz = new NodoA(cont, valorInsertar);
        else
            raiz.insert(cont, valorInsertar);
        cont++;
    }

    public void recorridoPreorden() {
        ayudantePreorden(raiz);
    }

    private void ayudantePreorden(NodoA nodo) {
        if (nodo == null)
            return;
        System.out.printf("|%d|%d| ", nodo.indice, nodo.datos);
        ayudantePreorden(nodo.nodoIzq);
        ayudantePreorden(nodo.nodoDer);
    }

    public void recorridoInorden() {
        ayudanteInorden(raiz);
    }

    private void ayudanteInorden(NodoA nodo) {
        if (nodo == null)
            return;
        ayudanteInorden(nodo.nodoIzq);
        System.out.printf("|%d|%d| ", nodo.indice, nodo.datos);
        ayudanteInorden(nodo.nodoDer);
    }

    public void recorridoPostorden() {
        ayudantePostorden(raiz);
    }

    private void ayudantePostorden(NodoA nodo) {
        if (nodo == null)
            return;
        ayudantePostorden(nodo.nodoIzq);
        ayudantePostorden(nodo.nodoDer);
        System.out.printf("|%d|%d| ", nodo.indice, nodo.datos);
    }

    public int[] eliminarNodo(int valorEliminar, int[] valorN) {
        int[] aux = valorN;
        int newCont = 0, contArreglo = 0;
        int[] newValor = new int[aux.length - 1];
        raiz = null;

        while (newCont <= newValor.length - 1) {
            if (newCont == valorEliminar) {
                contArreglo++;
            }

            newValor[newCont] = valorN[contArreglo];

            insertarNodo(newCont, newValor[newCont]);
            System.out.printf("|%d|%d| ", newCont, newValor[newCont]);

            newCont++;
            contArreglo++;

        }
        System.out.println();
        return newValor;
    }
}
