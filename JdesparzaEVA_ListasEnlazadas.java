import java.util.Scanner;

public class JdesparzaEVA_ListasEnlazadas {
    public static Nodo inicio = new Nodo("Jordy");
    public static Nodo fin = inicio;
    public static int cont;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        fin = fin.siguiente = new Nodo(3);
        fin = fin.siguiente = new Nodo(9);
        fin = fin.siguiente = new Nodo("David");
        fin = fin.siguiente = new Nodo(2);
        fin = fin.siguiente = new Nodo(1);
        fin = fin.siguiente = new Nodo(8);
        fin = fin.siguiente = new Nodo("Alex");
        fin = fin.siguiente = new Nodo("Juan");
        fin = fin.siguiente = new Nodo("Pedro");

        imprimirDato();

        System.out.print("\nDesea eliminar un número\n\t'1' para eliminar\n\t'cualquier tecla' para salir del " +
                "programa\nIngrese opcion: ");
        String opcion = scanner.nextLine();

        while (opcion.equals("1")) {
            System.out.print("\nIngrese un valor del indice: ");
            int nEliminar = scanner.nextInt();

            if ((nEliminar > cont-1) || (nEliminar < 0)) {
                System.out.printf("\nElemento del indice a elimiar invalido.\n\tSolo exite un " +
                        "indice de %d\n", cont-1);
            } else {
                eliminarDato(nEliminar);

                if (cont-1 > 0) {
                    imprimirDato();

                    scanner.nextLine();
                    System.out.print("\nDesea eliminar un número\n\t'1' para eliminar\n\t'cualquier tecla' " +
                            "para salir del programa\nIngrese opcion: ");
                    opcion = scanner.nextLine();
                } else opcion = "";
            }
        }
        System.out.println("SALIDA DEL PROGRAMA CON EXITO...");
    }

    public static void imprimirDato() {
        Nodo aux = inicio;
        cont = 0;
        while (aux != null) {
            if (aux.siguiente != null) System.out.printf("|%s|===>", aux.valor);
            else System.out.printf("|%s|\n", aux.valor);
            aux = aux.siguiente;
            cont++;
        }
        System.out.printf("El tamaño del indice de la Lista enlazada es de %d\n", cont-1);
    }

    public static void eliminarDato(int nIngresado) {
        Nodo aux = inicio;
        int cont1 = 0; // contador que se usara solo en este metodo
        Nodo temp1 = null;

        while (aux != null) {
            if ((cont1 == nIngresado) && (nIngresado != cont-1)) aux = aux.siguiente;
            else if ((cont1 == nIngresado) && (nIngresado == cont-1) && (cont1 != 0)) aux = null;
            else if ((cont1 == nIngresado) && (nIngresado == cont-1) && (cont1 == 0)) {
                aux = null;
                inicio = null;
                System.out.println("\nLISTA ENLAZADA VACIA....");
            }

            if (aux != null) {
                if (cont1 == 0) inicio = temp1 = new Nodo(aux.valor);
                else {
                    temp1.siguiente = new Nodo(aux.valor);
                    temp1 = temp1.siguiente;
                }
            }

            if (aux != null) aux = aux.siguiente;
            cont1++;
        }
        if (cont1-1 >= 0) System.out.println("DATO ELIMINADO CORRECTAMENTE...\n");
        cont = cont1; // el contador global toma el valor del contador de este metodo
    }
}

class Nodo<T>{
    public T valor;
    public Nodo siguiente;

    public Nodo(T valor) {this(valor, null);}

    public Nodo(T valor, Nodo siguiente) {
        this.valor = valor;
        this.siguiente = siguiente;
    }
}
